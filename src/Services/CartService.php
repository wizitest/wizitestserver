<?php
namespace App\Services;

class CartService
{
    public static function getTotalPriceTaxInclude($products, $productsQtyById)
    {
        $totalPriceTaxIncluded = 0;
        foreach ($products as $product)
        {
            $taxIncludedPrice = $product->price_tax_excluded + $product->price_tax_excluded * 0.2;
            $totalPriceTaxIncluded += $taxIncludedPrice * $productsQtyById[$product->id];
        }
        return round($totalPriceTaxIncluded, 2);
    }
}
