<?php
namespace App\Services;

const SERVER_URL = "http://wizitest-server.herokuapp.com/images/products/";

class ProductService
{
    public static function buildProduct($product)
    {
        return [
            'id' => $product->id,
            'categoryId' => $product->category_id,
            'name' => $product->name,
            'shortDescription' => $product->short_description,
            'description' => $product->description,
            'priceTaxExcluded' => $product->price_tax_excluded,
            'priceTaxIncluded' => $product->price_tax_excluded * 0.2 + $product->price_tax_excluded,
            'options' => array_map(
                fn($productOption) => [
                    'id' => $productOption->getOption()->id,
                    'label' => $productOption->getOption()->label,
                    'category' => $productOption->getOption()->option_category,
                ], $product->getProductOptions()->toArray()),
            'images' => array_map(
                fn($image) => [
                    'id' => $image->id,
                    'filename' => $image->filename,
                    'path' => SERVER_URL . $image->path,
                ], $product->getImages()->toArray()),
            'quantity' => $product->quantity,
        ];
    }
}
