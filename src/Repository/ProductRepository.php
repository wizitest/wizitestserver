<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findPaginatedProducts(
        ?int $categoryId,
        ?array $optionIds,
        string $sortOrder,
        int $page,
        int $itemsPerPage
    )
    {
        // Build request
        $qb = $this->getEntityManager()->createQueryBuilder("findPaginatedProducts")
            ->select('p')
            ->from('App\Entity\Product', 'p')
            ->leftJoin('App\Entity\ProductOption', 'po', 'WITH', 'po.product = p.id')
            ->leftJoin('App\Entity\Option', 'o', 'WITH', 'o.id = po.option');

        if ($categoryId !== null && $categoryId !== "")
        {
            $qb->andWhere('p.category_id = ?1')
                ->setParameter(1, $categoryId);
        }

        if ($optionIds !== null && count($optionIds) > 0)
        {
            $qb->andWhere('o.id IN (?2)')
                ->setParameter(2, $optionIds);
        }

        $qb->orderBy('p.price_tax_excluded', $sortOrder);

        // Pagination
        $qb->setFirstResult($page * $itemsPerPage)
            ->setMaxResults($itemsPerPage);

        return new Paginator($qb->getQuery(), $fetchJoinCollection = true);
    }
}
