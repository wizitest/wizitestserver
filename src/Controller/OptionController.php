<?php
namespace App\Controller;

use App\Entity\Option;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class OptionController extends AbstractController
{
    /**
     * @Route("/option", methods={"GET"})
     */
    public function getOptions(): JsonResponse
    {
        // Get options that are used
        $options = $this->getDoctrine()
            ->getRepository(Option::class)
            ->findUsedOptions();

        // Build response
        $result = array();
        foreach ($options as $option)
        {
            $result[$option->option_category][] = $this->buildOption($option);
        }
        return new JsonResponse($result);
    }

    public static function buildOption($option)
    {
        return [
            'id' => $option->id,
            'label' => $option->label,
            'category' => $option->option_category,
            'order' => $option->option_order,
        ];
    }
}
