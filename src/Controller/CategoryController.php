<?php
namespace App\Controller;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", methods={"GET"})
     */
    public function getCategories(): JsonResponse
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        return new JsonResponse(
            array_map(fn($category) =>
                $this->_buildResponse($category),
                $categories
            )
        );
    }

    /**
     * @Route("/category/{id}", methods={"GET"})
     */
    public function getCategoryById($id): JsonResponse
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneById($id);

        if ($category === null)
        {
            throw new HttpException(404, "Category not found");
        }

        return new JsonResponse($this->_buildResponse($category));
    }

    private function _buildResponse($category)
    {
        return [
            "id" => $category->id,
            "categoryName" => $category->category_name,
        ];
    }
}
