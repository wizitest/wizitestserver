<?php
namespace App\Controller;

use App\Entity\Product;
use App\Services\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", methods={"GET"})
     */
    public function getProducts(): JsonResponse
    {
        // Get request parameters
        $request = Request::createFromGlobals();
        $sortOrder = $request->query->get('sortBy');
        $categoryId = $request->query->get('categoryId');
        $optionIds = $request->query->all('options');
        $itemsPerPage = $request->query->get('itemsPerPage');
        $page = $request->query->get('page');

        // Check if params are valid, otherwise set default values
        if ($sortOrder === null || ($sortOrder !== "ASC" && $sortOrder !== "DESC"))
        {
            $sortOrder = "ASC";
        }
        if ($itemsPerPage === null)
        {
            $itemsPerPage = 5;
        }
        if ($page === null)
        {
            $page = 0;
        }

        $paginatedProducts = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findPaginatedProducts($categoryId, $optionIds, $sortOrder, $page, $itemsPerPage);

        // Build response
        $products = [];
        foreach ($paginatedProducts as $product)
        {
            $products[] = ProductService::buildProduct($product);
        }
        return new JsonResponse(
            [
                'pagination' => [
                    'page' => $page,
                    'itemsPerPage' => $itemsPerPage,
                    'totalNb' => count($paginatedProducts),
                ],
                'products' => $products,
            ]
        );
    }

    /**
     * @Route("/product/{id}", methods={"GET"})
     */
    public function getProductById($id): JsonResponse
    {
        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);

        if ($product === null)
        {
            throw new HttpException(404, 'Product not found');
        }

        return new JsonResponse(ProductService::buildProduct($product));
    }
}
