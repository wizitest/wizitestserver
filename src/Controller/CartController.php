<?php
namespace App\Controller;

use App\Entity\Product;
use App\Services\CartService;
use App\Services\ProductService;
use App\Services\StripePaymentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", methods={"POST"})
     */
    public function computeCart(Request $request): JsonResponse
    {
        $request = Request::createFromGlobals();
        $startPayment = $request->query->get('startPayment');
        $productsQtyById = $request->toArray();
        $productIds = array_keys($productsQtyById);

        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findById($productIds);

        $cart = new \stdClass();
        foreach ($products as $product)
        {
            $product->quantity = $productsQtyById[$product->id];
            $cart->products[] = ProductService::buildProduct($product);
        }

        $cart->totalPriceTaxIncluded = CartService::getTotalPriceTaxInclude($products, $productsQtyById);

        if ($startPayment === "true")
        {
            $cart->stripePaymentIntentSecret = StripePaymentService::generatePaymentSecret($cart->totalPriceTaxIncluded);
        }

        return new JsonResponse($cart);
    }
}
