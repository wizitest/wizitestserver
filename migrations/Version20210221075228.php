<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210221075228 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_option DROP FOREIGN KEY FK_38FA411446AF233F');
        $this->addSql('ALTER TABLE product_option DROP FOREIGN KEY FK_38FA4114DE18E50B');
        $this->addSql('DROP INDEX IDX_38FA4114DE18E50B ON product_option');
        $this->addSql('DROP INDEX IDX_38FA411446AF233F ON product_option');
        $this->addSql('ALTER TABLE product_option ADD product_id INT NOT NULL, ADD option_id INT NOT NULL, DROP product_id_id, DROP option_id_id');
        $this->addSql('ALTER TABLE product_option ADD CONSTRAINT FK_38FA41144584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_option ADD CONSTRAINT FK_38FA4114A7C41D6F FOREIGN KEY (option_id) REFERENCES `option` (id)');
        $this->addSql('CREATE INDEX IDX_38FA41144584665A ON product_option (product_id)');
        $this->addSql('CREATE INDEX IDX_38FA4114A7C41D6F ON product_option (option_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_option DROP FOREIGN KEY FK_38FA41144584665A');
        $this->addSql('ALTER TABLE product_option DROP FOREIGN KEY FK_38FA4114A7C41D6F');
        $this->addSql('DROP INDEX IDX_38FA41144584665A ON product_option');
        $this->addSql('DROP INDEX IDX_38FA4114A7C41D6F ON product_option');
        $this->addSql('ALTER TABLE product_option ADD product_id_id INT NOT NULL, ADD option_id_id INT NOT NULL, DROP product_id, DROP option_id');
        $this->addSql('ALTER TABLE product_option ADD CONSTRAINT FK_38FA411446AF233F FOREIGN KEY (option_id_id) REFERENCES `option` (id)');
        $this->addSql('ALTER TABLE product_option ADD CONSTRAINT FK_38FA4114DE18E50B FOREIGN KEY (product_id_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_38FA4114DE18E50B ON product_option (product_id_id)');
        $this->addSql('CREATE INDEX IDX_38FA411446AF233F ON product_option (option_id_id)');
    }
}
