## Installation

```
composer install
```

## Lancement

```
symfony server:start
```

Le serveur écoutera sur http://localhost:8000/
